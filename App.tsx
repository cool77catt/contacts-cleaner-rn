/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  FlatList,
  ListRenderItem,
} from 'react-native';
import {Card, Text, Button} from 'react-native-paper';
import Contacts, {Contact} from 'react-native-contacts';
import {getDeviceName} from 'react-native-device-info';

type RenderItemType = {
  name: string;
  contacts: Contact[];
};

const App = () => {
  const [duplicates, setDuplicates] = useState(new Map<string, Contact[]>());
  const [deviceName, setDeviceName] = useState('');
  const [listRefreshing, setListRefreshing] = useState(false);

  const refreshList = () => {
    Contacts.getAll()
      .then(contactsList => {
        let trackingMap = new Map<string, string[]>();
        let localDuplicates = new Map<string, Contact[]>();

        contactsList.forEach(ref => {
          // Search for duplicates
          const mapKey = ref.givenName + ' ' + ref.familyName;
          if (trackingMap.has(mapKey)) {
            let numbers = trackingMap.get(mapKey);
            let isDuplicate = true;
            ref.phoneNumbers.forEach(val => {
              if (!numbers!.includes(val.number)) {
                isDuplicate = false;
                numbers?.push(val.number);
              }
            });

            if (isDuplicate) {
              if (localDuplicates.has(mapKey)) {
                localDuplicates.get(mapKey)?.push(ref);
              } else {
                localDuplicates.set(mapKey, [ref]);
              }
            } else {
              // Add new numbers
              trackingMap.set(mapKey, numbers!);
            }
          } else {
            // Key doesn't exist, add to map
            trackingMap.set(
              mapKey,
              ref.phoneNumbers.map(val => val.number),
            );
          }
        });

        // Set the new duplicates list
        setDuplicates(localDuplicates);
        setListRefreshing(false);
      })
      .catch(err => console.log('Error', err.message));
  };

  useEffect(() => {
    // Get the device name
    getDeviceName().then(devName => setDeviceName(devName));

    // Get the list
    refreshList();
  }, []);

  const deleteContacts = (contacts: Contact[], refreshAfter = true) => {
    contacts.forEach((contact, idx) => {
      // console.log('Delete', contact.givenName + ' ' + contact.familyName);
      Contacts.deleteContact(contact).then(() => {
        // eslint-disable-next-line prettier/prettier
        if (refreshAfter && idx === (contacts.length - 1)) {
          refreshList();
        }
      });
    });
  };

  const deleteAllContacts = () => {
    for (let contacts of duplicates.values()) {
      deleteContacts(contacts, false);
    }
    refreshList();
  };

  const items = Array.from(duplicates.keys())
    .sort()
    .map<RenderItemType>(v => ({name: v, contacts: duplicates.get(v)!}));

  const renderItem: ListRenderItem<RenderItemType> = ({item}) => {
    return (
      <Card>
        <Card.Title
          title={item.name}
          subtitle={`Count: ${item.contacts.length}`}
          right={() => (
            <Button
              mode="contained"
              style={styles.cardDeleteButton}
              onPress={() => deleteContacts(item.contacts)}>
              Delete
            </Button>
          )}
        />
      </Card>
    );
  };

  const onListRefresh = () => {
    setListRefreshing(true);
    refreshList();
  };

  return (
    <SafeAreaView style={styles.safeAreaContainer}>
      <View style={styles.mainContainer}>
        <Text style={styles.headingText}>{deviceName}</Text>
        <Text style={styles.subtitleText}>Duplicate Contacts</Text>
        <FlatList
          data={items}
          renderItem={renderItem}
          keyExtractor={(item: RenderItemType) => item.name}
          style={styles.listContainer}
          refreshing={listRefreshing}
          onRefresh={onListRefresh}
        />
        <Button
          mode="contained"
          style={styles.deleteAllButton}
          onPress={deleteAllContacts}>
          Delete All
        </Button>
      </View>
    </SafeAreaView>
  );
};

const mainContainerBaseStyle: StyleSheet.NamedStyles<ViewStyle> = {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: 'white',
};

const styles = StyleSheet.create({
  safeAreaContainer: {
    ...mainContainerBaseStyle,
  },
  mainContainer: {
    ...mainContainerBaseStyle,
    width: '100%',
  },
  listContainer: {
    paddingTop: 16,
    width: '100%',
  },
  headingText: {
    fontSize: 32,
    fontWeight: 'bold',
  },
  subtitleText: {
    fontSize: 16,
  },
  cardDeleteButton: {
    marginRight: 16,
  },
  deleteAllButton: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 48,
    width: '60%',
  },
});

export default App;
